EAPI=7

inherit acct-user

DESCRIPTION="User for DV-Runtime"
ACCT_USER_ID=-1
ACCT_USER_GROUPS=( "dv-runtime" "usb" "plugdev" "video" )
ACCT_USER_HOME="/var/lib/dv-runtime"
ACCT_USER_HOME_OWNER="dv-runtime:dv-runtime"
ACCT_USER_HOME_PERMS="0755"

acct-user_add_deps
