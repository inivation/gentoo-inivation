# Copyright 2021 iniVation AG
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake git-r3

DESCRIPTION="Generic algorithms for event cameras."
HOMEPAGE="https://gitlab.com/inivation/dv/internal/${PN}-internal/"

EGIT_REPO_URI="https://gitlab.com/inivation/dv/internal/${PN}-internal.git"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~arm64 ~arm"
IUSE="debug test python"

RDEPEND="!<dev-util/dv-runtime-1.5.3
	>=dev-libs/boost-1.78.0
	>=media-libs/opencv-4.5.5
	>=dev-cpp/eigen-3.4.0
	>=dev-libs/libcaer-3.3.14
	>=app-arch/lz4-1.9.0
	>=app-arch/zstd-1.5.0
	>=dev-libs/libfmt-8.1.1
	dev-libs/openssl
	python? (
		>=dev-lang/python-3.8.0
		dev-python/numpy
	)"

DEPEND="${RDEPEND}
	virtual/pkgconfig
	>=dev-build/cmake-3.22.0"

src_configure() {
	local mycmakeargs=(
		-DDVP_ENABLE_TESTS="$(usex test ON OFF)"
		-DDVP_ENABLE_SAMPLES=OFF
		-DDVP_ENABLE_UTILITIES=ON
		-DDVP_ENABLE_BENCHMARKS=OFF
		-DDVP_ENABLE_PYTHON="$(usex python ON OFF)"
	)

	cmake_src_configure
}
