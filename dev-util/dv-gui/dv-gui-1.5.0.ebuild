# Copyright 2019 iniVation AG
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Java GUI for iniVation's Dynamic Vision (DV) C++ framework."
HOMEPAGE="https://gitlab.com/inivation/dv/${PN}/"

SRC_URI="http://release.inivation.com/gui/${PN}-linux-${PV}.tar.gz"

LICENSE="Redistributable"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

RDEPEND="
	dev-util/dv-runtime
	dev-util/flashy
	media-libs/freetype:2=
	media-libs/alsa-lib
	x11-libs/libX11
	x11-libs/libXext
	x11-libs/libXi
	x11-libs/libXrender
	x11-libs/libXtst"

DEPEND=""

src_install() {
	mkdir -p "${D}"/usr/share/pixmaps/
	mv dv-gui.png "${D}"/usr/share/pixmaps/

	mkdir -p "${D}"/usr/share/applications/
	mv dv-gui.desktop "${D}"/usr/share/applications/

	mkdir -p "${D}"/usr/bin/
	mv dv-gui "${D}"/usr/bin/

	mkdir -p "${D}"/opt/dv-gui/
	mv * "${D}"/opt/dv-gui/
}
