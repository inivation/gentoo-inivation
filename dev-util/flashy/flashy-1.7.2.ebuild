# Copyright 2019 iniVation AG
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Java tool to handle firmware and logic flashing for iniVation devices."
HOMEPAGE="https://gitlab.com/inivation/dv/${PN}/"

SRC_URI="http://release.inivation.com/${PN}/${PN}-linux-${PV}.zip"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

RDEPEND="
	media-libs/freetype:2=
	media-libs/alsa-lib
	x11-libs/libX11
	x11-libs/libXext
	x11-libs/libXi
	x11-libs/libXrender
	x11-libs/libXtst"

DEPEND=""

S="${WORKDIR}/${PN}-linux-${PV}"

src_install() {
	mkdir -p "${D}"/usr/share/pixmaps/
	mv flashy.png "${D}"/usr/share/pixmaps/

	mkdir -p "${D}"/usr/share/applications/
	mv flashy.desktop "${D}"/usr/share/applications/

	mkdir -p "${D}"/opt/flashy/
	mv * "${D}"/opt/flashy/

	mkdir -p "${D}"/usr/bin/
	ln -s /opt/flashy/bin/flashy "${D}"/usr/bin/flashy
}
